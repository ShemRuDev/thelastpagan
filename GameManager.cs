﻿using System;
using System.Collections;
using System.Collections.Generic;
using Stopwatch = System.Diagnostics.Stopwatch;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    // Public Fields that should be available everywhere
    public BoardManager BoardScript;
    [HideInInspector]
    public UIManager UIManager;
    [HideInInspector]
    public GameObject FOVPanel;
    [HideInInspector]
    public GameObject Player;

    // Game Wide variables
    public float TurnDelay = 0.1f;

    // State
    [HideInInspector]
    public GameState CurrentGameState = GameState.PlayerTurn;

    // Private fields
    private List<Enemy> _enemies;
    private bool _coEnemiesMoving; // Here and after _co* fields means running Coroutines

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        BoardScript = GetComponent<BoardManager>();
        UIManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        FOVPanel = GameObject.Find("FOVPanel");

        _enemies = new List<Enemy>();

        InitGame();
    }

    private void InitGame()
    {
        _enemies.Clear();

        BoardScript.SetupScene();
    }

    public void GameOver()
    {
        // Oh man please Don't forget about Morgue!
        Debug.Log("Your Game is OVER!");

        enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentGameState == GameState.PlayerTurn || _coEnemiesMoving)
            return;

        StartCoroutine(MovingEnemies());
    }

    public void AddEnemyToList(Enemy script)
    {
        _enemies.Add(script);
    }

    public void RemoveEnemy(string enemyName)
    {
        _enemies.RemoveAt(_enemies.FindIndex(e => e.Name == enemyName));
    }

    IEnumerator MovingEnemies()
    {
        _coEnemiesMoving = true;
        yield return new WaitForSeconds(TurnDelay);

        if(_enemies.Count == 0)
            yield return new WaitForSeconds(TurnDelay);

        //var sw = new Stopwatch();
        //sw.Start();
        for (int i = 0; i < _enemies.Count; i++)
        {
            _enemies[i].MoveEnemy();
            yield return new WaitForSeconds(_enemies[i].MoveTime);
        }
        //sw.Stop();
        //Debug.Log($"Enemies Movement taken: {sw.ElapsedMilliseconds} ms");

        CurrentGameState = GameState.PlayerTurn;
        _coEnemiesMoving = false;
    }
}
