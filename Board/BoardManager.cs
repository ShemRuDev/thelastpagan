﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using Debug = UnityEngine.Debug;

public class BoardManager : MonoBehaviour
{
    // Helper wrapper for RNG limits
    [Serializable]
    public class Count
    {
        public int Minimum;
        public int Maximum;

        public Count(int min, int max)
        {
            Minimum = min;
            Maximum = max;
        }
    }

    // Border Geometry
    public int Columns = 100;
    public int Rows = 100;
    public LayerMask BlockingLayer;

    // Objects on Map
    public Count ItemsCount = new Count(1, 3);
    public Count EnemiesCount = new Count(10, 20);
    public Count RiversCount = new Count(1, 3);

    // Prefabs
    public GameObject PlayerPrefab;
    public GameObject RiverPrefab;
    public GameObject[] GroundTiles;
    public GameObject[] ItemTiles;
    public GameObject[] EnemyTiles;
    
    // Map API
    [HideInInspector]
    public GameObject[,] AllPlacedGroundTiles;    // All Placed GameObjects-Tiles

    // FOV
    [HideInInspector]
    public bool[,] RevealedGrid;

    // Pathfinding
    [HideInInspector]
    public AStarPathfinding Pathfinding;

    // Private fields
    private FOV _playerFOV;
    private GroundTileNPManager _gtnpManager;
    private Dictionary<int, GroundTileType> _leftTempGeneratedPositions = new Dictionary<int, GroundTileType>();
    private GroundTileType _prevDownTileTypeCache = GroundTileType.Unknown;
    private List<Vector2Int> _allWaterTiles;
    private Transform _boardHolder;

    // DEBUG
    public bool DEBUG_GenerateRivers = true;

    public void SetupScene()
    {
        _playerFOV = GameManager.Instance.FOVPanel.GetComponent<FOV>();

        RevealedGrid = new bool[Columns, Rows];

        _gtnpManager = new GroundTileNPManager();
        _gtnpManager.Initialize();

        var sw = new Stopwatch();
        sw.Start();
        BoardSetup();
        sw.Stop();
        Debug.Log($"Board Setup took {sw.ElapsedMilliseconds} ms.");
        //Debug.Log($"Pure random hits: {_DEBUG_pureRandomHits}");
        //Debug.Log($"Water tiles count: {_allWaterTiles.Count}");

        PlacePlayer();

        // Init Listeners
        ItemsHolder.HolderDestroyed += ItemsHolderDestroyed;
        MovingObject.PlayerMoving += PlayerMoving;

        // Should be called only after All Terrain generated
        Pathfinding = new AStarPathfinding(AllPlacedGroundTiles);

        // Placing Objects
        //PlaceObjectAtRandom(ItemTiles, ItemsCount.Minimum, ItemsCount.Maximum);
        PlaceObjectAtRandom(EnemyTiles, EnemiesCount.Minimum, EnemiesCount.Maximum);
    }

    private void BoardSetup()
    {
        _boardHolder = new GameObject("Board").transform;
        AllPlacedGroundTiles = new GameObject[Columns, Rows];
        _allWaterTiles = new List<Vector2Int>();

        // Initial Ground tiles layout
        for (int x = 0; x < Columns; x++)
        {
            // Swap direction of Y generation for improving realistic terrain
            int startY = x % 2 == 0 ? 0 : Rows - 1;
            if (startY == 0)
                for (int y = startY; y < Rows; y++)
                {
                    var tileToInst = GetRandomGroundTile(x, y);
                    GameObject tileInst = Instantiate(tileToInst, new Vector3(x, y, 0f), Quaternion.identity);
                    tileInst.transform.SetParent(_boardHolder);

                    AllPlacedGroundTiles[x, y] = tileInst;
                }
            else
                for (int y = startY; y >= 0; y--)
                {
                    var tileToInst = GetRandomGroundTile(x, y);
                    GameObject tileInst = Instantiate(tileToInst, new Vector3(x, y, 0f), Quaternion.identity);
                    tileInst.transform.SetParent(_boardHolder);

                    AllPlacedGroundTiles[x, y] = tileInst;
                }
        }

        // Water tiles Rotating/Bordering
        LayoutWaterTiles();

        // Generate Rivers
        if (DEBUG_GenerateRivers)
            GenerateRivers();
    }

    #region EventHandlers
    
    private void ItemsHolderDestroyed(Vector2Int holderPos)
    {
        //Debug.Log($"Something at [{holderPos.x},{holderPos.y}] was destroyed");

        var itemHolderComp = AllPlacedGroundTiles[holderPos.x, holderPos.y].GetComponent<ItemsHolder>();
        var tileComp = AllPlacedGroundTiles[holderPos.x, holderPos.y].GetComponent<Tile>();
        bool wasHolderBlockVision = tileComp == null ? false : tileComp.BlockVision;

        GameObject itemToDrop = null;

        if (itemHolderComp != null)
            itemToDrop = itemHolderComp.GetRandomItem();

        PrepareTile(holderPos);

        if(itemToDrop != null)
        {
            var itemInst = Instantiate(itemToDrop, new Vector3(holderPos.x, holderPos.y, 0), Quaternion.identity);
            itemInst.transform.SetParent(_boardHolder);
        }

        if (wasHolderBlockVision)
        {
            //Debug.Log("Redraw FOV");
            var playerPos = GameManager.Instance.Player.transform.position;
            _playerFOV.Recalculate((int)playerPos.x, (int)playerPos.y);
            _playerFOV.Redraw((int)playerPos.x, (int)playerPos.y);
        }
    }

    private void PlayerMoving(Vector2Int targetPos)
    {
        //var sw = new Stopwatch();
        //sw.Start();
        _playerFOV.Recalculate(targetPos.x, targetPos.y);
        //Debug.Log($"FOV Recalculate taken {sw.ElapsedMilliseconds} ms.");

        //sw.Restart();
        _playerFOV.Redraw(targetPos.x, targetPos.y);
        //sw.Stop();
        //Debug.Log($"FOV Redraw taken {sw.ElapsedMilliseconds} ms.");
    }

    #endregion

    #region Initial Terrain Generation

    //private int _DEBUG_pureRandomHits = 0;
    private GameObject GetRandomGroundTile(int x, int y)
    {
        // Clear cache
        if (y == 0 || y == Rows - 1)
            _prevDownTileTypeCache = GroundTileType.Unknown;

        // Checking neighbours to calculate probabilities
        // Caller Loop is going One-by-one incrementing/decrementing 'y' in inner loop 
        // So we never ever will have Top Neighbour & Same true for Right tile
        var leftTileType = x == 0 ? GroundTileType.Unknown : _leftTempGeneratedPositions[y];
        var downTileType = _prevDownTileTypeCache;

        GameObject resultTile = GroundTiles[0];

        // This is the case for the First tile in the Location
        if (leftTileType == GroundTileType.Unknown && downTileType == GroundTileType.Unknown)
            return GetResultTile(GroundTiles, new Vector2Int(x, y));

        // So basically we have 95% probs for important neighbours and 5% for all others
        int maxRoll = 100;
        Dictionary<GroundTileType, int> mergedDict = null;
        if(downTileType != GroundTileType.Unknown)
            mergedDict = new Dictionary<GroundTileType, int>(_gtnpManager.GetNeighboursProbabilities(downTileType));
        if(leftTileType != GroundTileType.Unknown)
        {
            var leftNeighboursDict = _gtnpManager.GetNeighboursProbabilities(leftTileType);
            // Only Left tile exist (for Y = 0)
            if (mergedDict == null)
                mergedDict = new Dictionary<GroundTileType, int>(leftNeighboursDict);
            else
            {
                maxRoll = 200;
                foreach (var neProb in leftNeighboursDict)
                {
                    if (mergedDict.ContainsKey(neProb.Key))
                        mergedDict[neProb.Key] += neProb.Value;
                    else
                        mergedDict.Add(neProb.Key, neProb.Value);
                }
            }
        }

        // Get tile from merged Dict
        int roll = Random.Range(0, maxRoll);
        int accumulatedProb = 0;
        foreach (var neProb in mergedDict)
        {
            accumulatedProb += neProb.Value;
            // FOUND
            if (roll < accumulatedProb)
            {
                resultTile = GetResultTile(GroundTiles, new Vector2Int(x, y), neProb.Key);
                break;
            }
            
            // Just any random tile. From those that not in mergedDict
            if((maxRoll == 100 && accumulatedProb >= 95) || (maxRoll == 200 && accumulatedProb >= 190))
            {
                //_DEBUG_pureRandomHits++;
                var remainedTiles = GroundTiles
                    .Where(gt => !mergedDict.ContainsKey(gt.GetComponent<Tile>().TileType))
                    .ToArray();
                resultTile = GetResultTile(remainedTiles, new Vector2Int(x, y));
                break;
            }
        }

        return resultTile;
    }

    private GameObject GetResultTile(GameObject[] groundTiles, Vector2Int tilePosition, GroundTileType? typeKey = null)
    {
        var randomTile = typeKey.HasValue
            ? groundTiles.First(t => t.GetComponent<Tile>().TileType == typeKey.Value)
            : groundTiles[Random.Range(0, groundTiles.Length)];

        GroundTileType chosenTileType = randomTile.GetComponent<Tile>().TileType;
        _prevDownTileTypeCache = chosenTileType;
        _leftTempGeneratedPositions[tilePosition.y] = chosenTileType;

        if (chosenTileType == GroundTileType.WaterFull)
            _allWaterTiles.Add(tilePosition);

        return randomTile;
    }

    #endregion

    #region Polish Water Tiles

    // WTN - Stands for Water Tile Neighbour
    private void LayoutWaterTiles()
    {
        //var zRotationList = new List<int> { 0, 90, 180, 270 };

        foreach (Vector2Int wtPosition in _allWaterTiles)
        {
            int x = wtPosition.x;
            int y = wtPosition.y;

            // Get Neighbours and counts
            Tuple<int, int> orthoAndDiagWTNCount;
            bool[] wtnFlags = GetNeighboursFlagsAndCount(x, y, out orthoAndDiagWTNCount);
            int wtnCount = orthoAndDiagWTNCount.Item1 + orthoAndDiagWTNCount.Item2;

            // Skip sprite swapping if All Neighbours are Water tiles
            if (wtnCount == 8)
                continue;

            var spriteAndRotation = GetSpriteAndRotationToEnable(orthoAndDiagWTNCount, wtnFlags);
            string spriteToEnableName = spriteAndRotation.Item1;
            int zRotation = spriteAndRotation.Item2;
            bool xFlip = spriteAndRotation.Item3;
            bool yFlip = spriteAndRotation.Item4;

            if (string.IsNullOrEmpty(spriteToEnableName))
                continue;

            AllPlacedGroundTiles[x, y].transform.rotation = Quaternion.Euler(0, 0, zRotation);
            AllPlacedGroundTiles[x, y].GetComponent<SpriteRenderer>().enabled = false;
            var sprites = AllPlacedGroundTiles[x, y].gameObject.GetComponentsInChildren<SpriteRenderer>(true);

            var spriteToEnable = sprites.First(s => s.name == spriteToEnableName);
            spriteToEnable.enabled = true;
            spriteToEnable.flipX = xFlip;
            spriteToEnable.flipY = yFlip;
        }
    }

    private bool[] GetNeighboursFlagsAndCount(int x, int y, out Tuple<int, int> orthoAndDiagWTNCount)
    {
        GroundTileType leftTile = x == 0 ? GroundTileType.Unknown : AllPlacedGroundTiles[x - 1, y].GetComponent<Tile>().TileType;
        GroundTileType rightTile = x == Columns - 1 ? GroundTileType.Unknown : AllPlacedGroundTiles[x + 1, y].GetComponent<Tile>().TileType;
        GroundTileType topTile = y == Rows - 1 ? GroundTileType.Unknown : AllPlacedGroundTiles[x, y + 1].GetComponent<Tile>().TileType;
        GroundTileType downTile = y == 0 ? GroundTileType.Unknown : AllPlacedGroundTiles[x, y - 1].GetComponent<Tile>().TileType;

        GroundTileType leftTopTile = x == 0 || y == Rows - 1
            ? GroundTileType.Unknown
            : AllPlacedGroundTiles[x - 1, y + 1].GetComponent<Tile>().TileType;
        GroundTileType rightTopTile = x == Columns - 1 || y == Rows - 1
            ? GroundTileType.Unknown
            : AllPlacedGroundTiles[x + 1, y + 1].GetComponent<Tile>().TileType;
        GroundTileType leftDownTile = x == 0 || y == 0
            ? GroundTileType.Unknown
            : AllPlacedGroundTiles[x - 1, y - 1].GetComponent<Tile>().TileType;
        GroundTileType rightDownTile = x == Columns - 1 || y == 0
            ? GroundTileType.Unknown
            : AllPlacedGroundTiles[x + 1, y - 1].GetComponent<Tile>().TileType;

        // Count Neighbours
        // [L] [LT] [T] [RT] [R] [RD] [D] [LD]
        //  0   1    2   3    4   5    6   7
        bool[] waterTileNbrsFlags = new bool[8];
        int orthoWtnCount = 0;
        int diagWtnCount = 0;
        if (leftTile == GroundTileType.WaterFull)
        {
            orthoWtnCount++;
            waterTileNbrsFlags[0] = true;
        }
        if (rightTile == GroundTileType.WaterFull)
        {
            orthoWtnCount++;
            waterTileNbrsFlags[4] = true;
        }
        if (topTile == GroundTileType.WaterFull)
        {
            orthoWtnCount++;
            waterTileNbrsFlags[2] = true;
        }
        if (downTile == GroundTileType.WaterFull)
        {
            orthoWtnCount++;
            waterTileNbrsFlags[6] = true;
        }
        if (leftTopTile == GroundTileType.WaterFull)
        {
            diagWtnCount++;
            waterTileNbrsFlags[1] = true;
        }
        if (leftDownTile == GroundTileType.WaterFull)
        {
            diagWtnCount++;
            waterTileNbrsFlags[7] = true;
        }
        if (rightTopTile == GroundTileType.WaterFull)
        {
            diagWtnCount++;
            waterTileNbrsFlags[3] = true;
        }
        if (rightDownTile == GroundTileType.WaterFull)
        {
            diagWtnCount++;
            waterTileNbrsFlags[5] = true;
        }

        orthoAndDiagWTNCount = new Tuple<int, int>(orthoWtnCount, diagWtnCount);

        return waterTileNbrsFlags;
    }

    /// <param name="orthoAndDiagWTNCount"></param>
    /// <param name="wtnFlags"></param>
    /// <returns>Item1 - SpriteName to enable; Item2 - zRotation; Item3 - X flip; Item4 - Y flip</returns>
    private Tuple<string, int, bool, bool> GetSpriteAndRotationToEnable(Tuple<int, int> orthoAndDiagWTNCount, bool[] wtnFlags)
    {
        int orthoWtnCount = orthoAndDiagWTNCount.Item1;
        int diagWtnCount = orthoAndDiagWTNCount.Item2;
        int wtnCount = orthoWtnCount + diagWtnCount;

        string spriteToEnableName = "";
        int zRotation = 0;
        bool xFlip = false;
        bool yFlip = false;

        // NO Neighbours
        if (wtnCount == 0)
        {
            zRotation = 0;
            spriteToEnableName = "water_single";
        }
        // ONLY 1 Ortho Neighbour - Diags Excluded
        else if (orthoWtnCount == 1)
        {
            spriteToEnableName = "water_1";

            if (wtnFlags[0])
                zRotation = 270;
            else if (wtnFlags[4])
                zRotation = 90;
            else if (wtnFlags[2])
                zRotation = 180;
        }
        // 2 Ortho WTN
        else if (orthoWtnCount == 2)
        {
            // 1 Block
            if (wtnFlags[0] && wtnFlags[4])
            {
                spriteToEnableName = "water_2_UD";
                zRotation = 90;
            }
            else if (wtnFlags[2] && wtnFlags[6])
            {
                spriteToEnableName = "water_2_UD";
                zRotation = 0;
            }

            // 2 Block
            if (wtnFlags[0] && wtnFlags[2])
            {
                spriteToEnableName = "water_2_RD";
                zRotation = 180;
            }
            else if (wtnFlags[0] && wtnFlags[6])
            {
                spriteToEnableName = "water_2_RD";
                zRotation = 270;
            }
            else if (wtnFlags[2] && wtnFlags[4])
            {
                spriteToEnableName = "water_2_RD";
                zRotation = 90;
            }
            else if (wtnFlags[4] && wtnFlags[6])
            {
                spriteToEnableName = "water_2_RD";
                zRotation = 0;
            }

            // 3 Block
            if (wtnFlags[0] && wtnFlags[2] && wtnFlags[1])
            {
                spriteToEnableName = "water_3_RD_1Diag";
                zRotation = 180;
            }
            else if (wtnFlags[0] && wtnFlags[6] && wtnFlags[7])
            {
                spriteToEnableName = "water_3_RD_1Diag";
                zRotation = 270;
            }
            else if (wtnFlags[4] && wtnFlags[2] && wtnFlags[3])
            {
                spriteToEnableName = "water_3_RD_1Diag";
                zRotation = 90;
            }
            else if (wtnFlags[4] && wtnFlags[6] && wtnFlags[5])
            {
                spriteToEnableName = "water_3_RD_1Diag";
                zRotation = 0;
            }
        }
        // 3 Ortho WTN
        else if (orthoWtnCount == 3)
        {
            // Simplest - First
            if (wtnFlags[0] && wtnFlags[2] && wtnFlags[4])
            {
                spriteToEnableName = "water_3_URD_0Diag";
                zRotation = 90;
            }
            else if (wtnFlags[0] && wtnFlags[4] && wtnFlags[6])
            {
                spriteToEnableName = "water_3_URD_0Diag";
                zRotation = 270;
            }
            else if (wtnFlags[2] && wtnFlags[4] && wtnFlags[6])
            {
                spriteToEnableName = "water_3_URD_0Diag";
                zRotation = 0;
            }
            else if (wtnFlags[0] && wtnFlags[2] && wtnFlags[6])
            {
                spriteToEnableName = "water_3_URD_0Diag";
                zRotation = 180;
            }

            // Next block
            if (wtnFlags[2] && wtnFlags[4] && wtnFlags[6])
            {
                if (wtnFlags[3])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 0;
                }
                else if (wtnFlags[5])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 0;
                    yFlip = true;
                }
            }
            else if (wtnFlags[4] && wtnFlags[6] && wtnFlags[0])
            {
                if (wtnFlags[5])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 270;
                }
                else if (wtnFlags[7])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 270;
                    yFlip = true;
                }
            }
            else if (wtnFlags[6] && wtnFlags[0] && wtnFlags[2])
            {
                if (wtnFlags[7])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 180;
                }
                else if (wtnFlags[1])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 180;
                    yFlip = true;
                }
            }
            else if (wtnFlags[0] && wtnFlags[2] && wtnFlags[4])
            {
                if (wtnFlags[1])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 90;
                }
                else if (wtnFlags[3])
                {
                    spriteToEnableName = "water_4_URD_1Diag";
                    zRotation = 90;
                    yFlip = true;
                }
            }

            // hardest
            if (wtnFlags[2] && wtnFlags[4] && wtnFlags[6] && wtnFlags[3] && wtnFlags[5])
            {
                spriteToEnableName = "water_5_URD_2Diag";
                zRotation = 0;
            }
            else if (wtnFlags[4] && wtnFlags[6] && wtnFlags[0] && wtnFlags[5] && wtnFlags[7])
            {
                spriteToEnableName = "water_5_URD_2Diag";
                zRotation = 270;
            }
            else if (wtnFlags[6] && wtnFlags[0] && wtnFlags[2] && wtnFlags[7] && wtnFlags[1])
            {
                spriteToEnableName = "water_5_URD_2Diag";
                zRotation = 180;
            }
            else if (wtnFlags[0] && wtnFlags[2] && wtnFlags[4] && wtnFlags[1] && wtnFlags[3])
            {
                spriteToEnableName = "water_5_URD_2Diag";
                zRotation = 90;
            }
        }
        // 4 (All) Ortho WTN
        else if(orthoWtnCount == 4)
        {
            if(diagWtnCount == 0)
            {
                spriteToEnableName = "water_4_URDL";
                zRotation = 0;
            }
            else
            {
                // 1 Diag block
                if (wtnFlags[1])
                {
                    spriteToEnableName = "water_5_4O_1Diag";
                    zRotation = 0;
                    xFlip = true;
                    yFlip = false;
                }
                else if (wtnFlags[3])
                {
                    spriteToEnableName = "water_5_4O_1Diag";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = false;
                }
                else if (wtnFlags[5])
                {
                    spriteToEnableName = "water_5_4O_1Diag";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = true;
                }
                else if (wtnFlags[7])
                {
                    spriteToEnableName = "water_5_4O_1Diag";
                    zRotation = 0;
                    xFlip = true;
                    yFlip = true;
                }

                // 2 Diags block
                if(wtnFlags[1] && wtnFlags[3])
                {
                    spriteToEnableName = "water_6_4O_2Diag1Side";
                    zRotation = 90;
                    xFlip = false;
                    yFlip = false;
                }
                else if (wtnFlags[5] && wtnFlags[7])
                {
                    spriteToEnableName = "water_6_4O_2Diag1Side";
                    zRotation = 90;
                    xFlip = true;
                    yFlip = false;
                }
                else if (wtnFlags[1] && wtnFlags[7])
                {
                    spriteToEnableName = "water_6_4O_2Diag1Side";
                    zRotation = 0;
                    xFlip = true;
                    yFlip = false;
                }
                else if (wtnFlags[3] && wtnFlags[5])
                {
                    spriteToEnableName = "water_6_4O_2Diag1Side";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = false;
                }
                else if (wtnFlags[1] && wtnFlags[5])
                {
                    spriteToEnableName = "water_6_4O_2DiagMirror";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = true;
                }
                else if (wtnFlags[3] && wtnFlags[7])
                {
                    spriteToEnableName = "water_6_4O_2DiagMirror";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = false;
                }

                // 3 Diags block
                if (wtnFlags[1] && wtnFlags[3] && wtnFlags[5])
                {
                    spriteToEnableName = "water_7";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = true;
                }
                else if (wtnFlags[3] && wtnFlags[5] && wtnFlags[7])
                {
                    spriteToEnableName = "water_7";
                    zRotation = 0;
                    xFlip = false;
                    yFlip = false;
                }
                else if (wtnFlags[5] && wtnFlags[7] && wtnFlags[1])
                {
                    spriteToEnableName = "water_7";
                    zRotation = 0;
                    xFlip = true;
                    yFlip = false;
                }
                else if (wtnFlags[7] && wtnFlags[1] && wtnFlags[3])
                {
                    spriteToEnableName = "water_7";
                    zRotation = 0;
                    xFlip = true;
                    yFlip = true;
                }
            }
        }

        return new Tuple<string, int, bool, bool>(spriteToEnableName, zRotation, xFlip, yFlip);
    }

    #endregion

    #region Rivers

    private void GenerateRivers()
    {
        int riversCount = Random.Range(RiversCount.Minimum, RiversCount.Maximum);
        for (int i = 0; i < riversCount; i++)
        {
            GenerateRiver();
        }
    }

    // Super basic implementation. No pathfinding. No collision checks & so on.
    // TODO: REFAC!
    private void GenerateRiver()
    {
        Vector2Int startPosition = new Vector2Int(0, Random.Range(0, Rows));
        int length = Random.Range(Columns, 2 * Columns);

        int currentFlowDirection = 0;   // 0 - x++; 1 - y--; 2 - x--; 3 - y++
        int currentX = startPosition.x;
        int currentY = startPosition.y;
        for (int i = 0; i < length; i++)
        {
            // First step is always forward
            if (i == 0)
            {
                Destroy(AllPlacedGroundTiles[currentX, currentY]);

                var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.Euler(0, 0, 90));
                riverInst.transform.SetParent(_boardHolder);
                AllPlacedGroundTiles[currentX, currentY] = riverInst;
                continue;
            }

            int turnDirection = 0;// -1: Down 0: No changes +1: Up
            int yDirChangeRoll = Random.Range(0, 100);
            int currentDirectionProb = currentFlowDirection == 0 ? 90 : 40; // trying to move forward
            if (yDirChangeRoll >= currentDirectionProb && yDirChangeRoll < currentDirectionProb + (100 - currentDirectionProb) / 2)
                turnDirection = -1;
            else if (yDirChangeRoll >= currentDirectionProb + (100 - currentDirectionProb) / 2)
                turnDirection = 1;

            if (currentFlowDirection == 0)
            {
                currentX++;

                if (currentX >= Columns)
                    break;

                if (turnDirection == 0)
                {
                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.Euler(0, 0, 90));
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if(turnDirection == -1)
                {
                    currentFlowDirection = 1;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipX = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == 1)
                {
                    currentFlowDirection = 3;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipX = true;
                    turnSprite.flipY = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
            }
            else if(currentFlowDirection == 1)
            {
                currentY--;

                if (currentY < 0)
                    break;

                if (turnDirection == 0)
                {
                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.Euler(0, 0, 0));
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == -1)
                {
                    currentFlowDirection = 2;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipX = true;
                    turnSprite.flipY = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == 1)
                {
                    currentFlowDirection = 0;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipY = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
            }
            else if (currentFlowDirection == 2)
            {
                currentX--;

                if (currentX < 0)
                    break;

                if (turnDirection == 0)
                {
                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.Euler(0, 0, 90));
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == -1)
                {
                    currentFlowDirection = 1;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == 1)
                {
                    currentFlowDirection = 3;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipY = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
            }
            else if (currentFlowDirection == 3)
            {
                currentY++;

                if (currentY >= Rows)
                    break;

                if (turnDirection == 0)
                {
                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.Euler(0, 0, 0));
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == -1)
                {
                    currentFlowDirection = 2;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    turnSprite.flipX = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
                else if (turnDirection == 1)
                {
                    currentFlowDirection = 0;

                    Destroy(AllPlacedGroundTiles[currentX, currentY]);
                    var riverInst = Instantiate(RiverPrefab, new Vector3(currentX, currentY, 0), Quaternion.identity);
                    riverInst.GetComponent<SpriteRenderer>().enabled = false;
                    var turnSprite = riverInst.GetComponentsInChildren<SpriteRenderer>(true).First(s => s.name == "river_turn");
                    turnSprite.enabled = true;
                    riverInst.transform.SetParent(_boardHolder);
                    AllPlacedGroundTiles[currentX, currentY] = riverInst;
                }
            }
        }
    }

    #endregion

    private void PlacePlayer()
    {
        // temporary at the center of camera FOV. (SHOULD - At the borders of location)
        var playerPosition = new Vector2Int(Columns / 2, Rows / 2);

        // Remove Blocking Spawn-point Tile
        PrepareTile(playerPosition);

        GameManager.Instance.Player = Instantiate(PlayerPrefab, new Vector3(playerPosition.x, playerPosition.y, 0), Quaternion.identity);
        
        // Player FOV Init
        var fov = GameManager.Instance.FOVPanel.GetComponent<FOV>();
        fov.Recalculate(playerPosition.x, playerPosition.y);
        fov.Redraw(playerPosition.x, playerPosition.y);
    }

    #region Utilities

    // For Placing objects with Name property only! (Yeah I know. But we Developing THE GAME Here. No time for Perfect Code!)
    private void PlaceObjectAtRandom(GameObject[] tiles, int minimum, int maximum)
    {
        int count = Random.Range(minimum, maximum + 1);

        //temp
        var playerPos = GameManager.Instance.Player.transform.position;

        for (int i = 0; i < count; i++)
        {
            // NB: For now we don't care about Spawning in the same position
            // FOR Pathfinding testing placing Enemy near player
            Vector2Int randomPosition;
            if (i == 0)
                randomPosition = new Vector2Int(Random.Range((int)playerPos.x - 5, (int)playerPos.x + 5), Random.Range((int)playerPos.y - 5, (int)playerPos.y + 5));
            else
                randomPosition = new Vector2Int(Random.Range(0, Columns), Random.Range(0, Rows));
            PrepareTile(randomPosition);

            var tileChoice = tiles[Random.Range(0, tiles.Length)];
            var generatedTile = Instantiate(tileChoice, new Vector3(randomPosition.x, randomPosition.y, 0f), Quaternion.identity);

            var enemy = generatedTile.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.Name = $"{enemy.Name}{i}";
                generatedTile.name = enemy.Name;
                GameManager.Instance.AddEnemyToList(enemy);
            }
        }
    }

    // Checking if we have something that blocks movement before actual placing on board
    private void PrepareTile(Vector2Int position, GameObject clearTile = null)
    {
        if (clearTile == null)
            clearTile = GroundTiles[0]; // Empty

        if (AllPlacedGroundTiles[position.x, position.y].GetComponent<Tile>().BlockMovement)
        {
            Destroy(AllPlacedGroundTiles[position.x, position.y]);

            var emptyInst = Instantiate(clearTile, new Vector3(position.x, position.y, 0), Quaternion.identity);
            emptyInst.transform.SetParent(_boardHolder);

            AllPlacedGroundTiles[position.x, position.y] = emptyInst;
        }
    }

    #endregion
}
