﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FOV : MonoBehaviour
{
    public GameObject FovTilePrefab;
    public LayerMask BlockingLayer;

    public const int MAX_RANGE = 5;
    private const int MAX_X_LOOKUP = 15;
    private const int MAX_Y_LOOKUP = 10;

    public bool DEBUG_IsRedraw = true;  // Helps for debug. Not Drawing Fog of War.

    private BoxCollider2D _playerCollider;
    private List<Vector2Int> _currentFov;
    
    public void Recalculate(int x, int y)
    {
        _currentFov = new List<Vector2Int>(MAX_RANGE * 10);
        _playerCollider = GameManager.Instance.Player.GetComponent<BoxCollider2D>();

        BoardManager board = GameManager.Instance.BoardScript;
        // the player moves here
        board.RevealedGrid[x, y] = true;
        
        CircleFOV(x, y, Vector2.left);
        CircleFOV(x, y, Vector2.right);
    }

    private void CircleFOV(int startX, int startY, Vector2 direction)
    {
        if(direction == Vector2.left)
        {
            for (int x = 0; x <= MAX_RANGE; x++)
            {
                int targetX = startX - x;

                for (int y = 0; y <= MAX_RANGE - x; y++)
                {
                    // No need to check player pos
                    if (x == 0 && y == 0)
                        continue;

                    int targetY = startY + y;

                    CheckVisibility(startX, startY, targetX, targetY);
                }

                for (int y = 1; y <= MAX_RANGE - x; y++)
                {
                    int targetY = startY - y;

                    CheckVisibility(startX, startY, targetX, targetY);
                }
            }
        }
        else if (direction == Vector2.right)
        {
            for (int x = 1; x <= MAX_RANGE; x++)
            {
                int targetX = startX + x;

                for (int y = 0; y <= MAX_RANGE - x; y++)
                {
                    int targetY = startY + y;

                    CheckVisibility(startX, startY, targetX, targetY);
                }

                for (int y = 1; y <= MAX_RANGE - x; y++)
                {
                    int targetY = startY - y;

                    CheckVisibility(startX, startY, targetX, targetY);
                }
            }
        }
    }

    private void CheckVisibility(int startX, int startY, int targetX, int targetY)
    {
        BoardManager board = GameManager.Instance.BoardScript;

        if (targetX < 0 || targetX > board.Columns - 1 || targetY < 0 || targetY > board.Rows - 1)
            return;

        _playerCollider.enabled = false;
        RaycastHit2D hit = Physics2D.Linecast(new Vector2(startX, startY), new Vector2(targetX, targetY), BlockingLayer);
        _playerCollider.enabled = true;
        // - No hits at all
        // - Hit is the first Collider/Blocker
        // - Hit dont have BlockVision Tile component
        if (hit.transform == null ||
            (hit.transform.position.x == targetX && hit.transform.position.y == targetY) ||
            (hit.transform.GetComponent<Tile>() != null && !hit.transform.GetComponent<Tile>().BlockVision))
        {
            board.RevealedGrid[targetX, targetY] = true;
            _currentFov.Add(new Vector2Int(targetX, targetY));
        }
    }

    // redraw based on position of camera 
    // (currentX and currentY most often is the center of the screen, where player stands)
    public void Redraw(int currentX, int currentY)
    {
        if (!DEBUG_IsRedraw)
            return;

        // redraw the Fog of War tiles
        List<GameObject> children = new List<GameObject>();
        // first we gather all children of FOVPanel
        foreach (Transform child in transform)
        {
            children.Add(child.gameObject);
        }

        // then we delete them
        foreach (GameObject child in children)
        {
            Destroy(child);
        }

        BoardManager board = GameManager.Instance.BoardScript;

        // then we draw new Fog of War tiles
        for (int x = -MAX_X_LOOKUP + currentX; x < MAX_X_LOOKUP + currentX + 1; x++)
        {
            for (int y = -MAX_Y_LOOKUP + currentY; y < MAX_Y_LOOKUP + currentY; y++)
            {
                if (x < 0 || y < 0 || x > board.Columns - 1 || y > board.Rows - 1)
                {
                    continue;
                }

                if (board.RevealedGrid[x, y] == false)
                {
                    Vector3 pos = new Vector2(x, y);
                    GameObject obj = Instantiate(FovTilePrefab, pos, Quaternion.identity) as GameObject;
                    obj.transform.parent = transform;
                }
                else
                {
                    Vector2Int distVector = new Vector2Int(currentX, currentY) - new Vector2Int(x, y);
                    int tilesDistance = Mathf.Abs(distVector.x) + Mathf.Abs(distVector.y);

                    if (tilesDistance > MAX_RANGE)
                    {
                        float newAlpha = 0.1f;

                        Vector3 pos = new Vector2(x, y);
                        GameObject obj = Instantiate(FovTilePrefab, pos, Quaternion.identity) as GameObject;

                        var fovSprite = obj.GetComponent<SpriteRenderer>();
                        var newColor = fovSprite.color;
                        newColor.a = Mathf.Clamp(newAlpha + (tilesDistance - MAX_RANGE) * 0.2f, 0f, 0.9f);
                        fovSprite.color = newColor;

                        obj.transform.parent = transform;
                    }
                }
            }
        }
    }

    public bool IsInFOV(Vector2Int position)
    {
        return _currentFov.Any(v => v.x == position.x && v.y == position.y);
    }

    public bool IsInFOV(Vector3 position)
    {
        return IsInFOV(new Vector2Int((int)position.x, (int)position.y));
    }
}
