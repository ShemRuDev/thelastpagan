﻿using System.Collections.Generic;

/// <summary>
/// Manages neighbours probabilities for tile.
/// </summary>
public class GroundTileNPManager
{
    private Dictionary<GroundTileType, Dictionary<GroundTileType, int>> _probabilitiesForTile;

    public void Initialize()
    {
        _probabilitiesForTile = new Dictionary<GroundTileType, Dictionary<GroundTileType, int>>();

        _probabilitiesForTile[GroundTileType.GroundEmpty] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundEmpty, 80 },
            { GroundTileType.GroundDirtS, 10 },
            { GroundTileType.GroundGrassS, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundDirtS] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundDirtS, 80 },
            { GroundTileType.GroundDirtM, 10 },
            { GroundTileType.GroundGrassM, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundDirtM] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundDirtM, 80 },
            { GroundTileType.GroundDirtL, 10 },
            { GroundTileType.GroundGrassAndFlowers, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundDirtL] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundDirtL, 80 },
            { GroundTileType.GroundDirtXL, 10 },
            { GroundTileType.GroundGrassAndFlowers, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundDirtXL] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundDirtXL, 60 },
            { GroundTileType.GroundDirtL, 20 },
            { GroundTileType.GroundGrassAndFlowers, 10 },
            { GroundTileType.ForestOak, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundGrassS] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundGrassS, 80 },
            { GroundTileType.GroundGrassM, 10 },
            { GroundTileType.GroundGrassAndFlowers, 5 }
        };
        _probabilitiesForTile[GroundTileType.GroundGrassAndFlowers] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundGrassAndFlowers, 80 },
            { GroundTileType.ForestOak, 10 },
            { GroundTileType.ForestFir, 5 },
        };
        _probabilitiesForTile[GroundTileType.GroundGrassM] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundGrassM, 80 },
            { GroundTileType.GroundGrassAndFlowers, 10 },
            { GroundTileType.ForestOak, 5 }
        };
        _probabilitiesForTile[GroundTileType.ForestOak] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.ForestOak, 80 },
            { GroundTileType.GroundEmpty, 10 },
            { GroundTileType.ForestFir, 5 }
        };
        _probabilitiesForTile[GroundTileType.ForestFir] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.ForestFir, 80 },
            { GroundTileType.GroundEmpty, 10 },
            { GroundTileType.ForestOak, 5 }
        };
        _probabilitiesForTile[GroundTileType.WaterFull] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.WaterFull, 90 },
            //{ GroundTileType.GroundDirtM, 7 },
            { GroundTileType.GroundGrassAndFlowers, 5 }
        };
        _probabilitiesForTile[GroundTileType.Unknown] = new Dictionary<GroundTileType, int>()
        {
            { GroundTileType.GroundEmpty, 100 }
        };
    }

    public Dictionary<GroundTileType, int> GetNeighboursProbabilities(GroundTileType typeKey)
    {
        Dictionary<GroundTileType, int> resultProbs;
        if (!_probabilitiesForTile.TryGetValue(typeKey, out resultProbs))
            resultProbs = _probabilitiesForTile[GroundTileType.Unknown];

        return resultProbs;
    }
}
