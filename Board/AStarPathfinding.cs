﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AStarPathfinding
{
    private GameObject[,] _board;
    private LayerMask _blockingLayer;

    public AStarPathfinding(GameObject[,] board)
    {
        _board = board;
    }

    public Vector2Int GetNextPointDirection(Vector2Int startPosition, Vector2Int targetPosition)
    {
        List<TileWithScores> openedList = new List<TileWithScores>();
        List<TileWithScores> closedList = new List<TileWithScores>();

        Vector2Int resultDirection = Vector2Int.zero;

        var targetTile = new TileWithScores(targetPosition, startPosition, targetPosition);
        //Debug.Log($"Target: {targetTile}");
        var startTile = new TileWithScores(startPosition, startPosition, targetPosition);
        //Debug.Log($"Start: {startTile}");
        openedList.Add(startTile);
        Vector2Int nextPointPosition = Vector2Int.zero;
        //int step = 0;
        do
        {
            //Debug.Log($"Step: {++step}");

            int currentTileIdx = openedList.FindIndex(t => t.Score == openedList.Min(ot => ot.Score));

            var currentTile = openedList[currentTileIdx];
            //Debug.Log($"Current Minimal F Tile:");
            //Debug.Log($"{currentTile}");

            //Debug.Log($"Opened list count: {openedList.Count}");

            closedList.Add(currentTile);
            openedList.RemoveAt(currentTileIdx);

            //Debug.Log($"Opened list count: {openedList.Count}");
            //Debug.Log($"Closed list count: {closedList.Count}");

            // Found PATH
            if (closedList.Any(t => t.Position.x == targetTile.Position.x && t.Position.y == targetTile.Position.y))
            {
                //Debug.Log($"Found PATH!");
                // Start backward movement
                TileWithScores tempTile = closedList.First(t => t.Equals(targetTile));
                TileWithScores prevStepTile = tempTile;
                do
                {
                    if (tempTile.ParentTile == null)
                    {
                        nextPointPosition = prevStepTile.Position;
                        break;
                    }
                    else
                    {
                        prevStepTile = tempTile;
                        tempTile = tempTile.ParentTile;
                    }
                } while (true);
                break;
            }

            // Get all not blocking adj tiles
            List<Vector2Int> passableAdjTiles = new List<Vector2Int>();
            if (currentTile.Position.x > 0 && 
                !_board[currentTile.Position.x - 1, currentTile.Position.y].GetComponent<Tile>().BlockMovement)
                passableAdjTiles.Add(new Vector2Int(currentTile.Position.x - 1, currentTile.Position.y));
            if (currentTile.Position.x < _board.GetLength(0) - 1 && 
                !_board[currentTile.Position.x + 1, currentTile.Position.y].GetComponent<Tile>().BlockMovement)
                passableAdjTiles.Add(new Vector2Int(currentTile.Position.x + 1, currentTile.Position.y));
            if (currentTile.Position.y > 0 && 
                !_board[currentTile.Position.x, currentTile.Position.y - 1].GetComponent<Tile>().BlockMovement)
                passableAdjTiles.Add(new Vector2Int(currentTile.Position.x, currentTile.Position.y - 1));
            if (currentTile.Position.y < _board.GetLength(1) - 1 && 
                !_board[currentTile.Position.x, currentTile.Position.y + 1].GetComponent<Tile>().BlockMovement)
                passableAdjTiles.Add(new Vector2Int(currentTile.Position.x, currentTile.Position.y + 1));

            foreach (Vector2Int pat in passableAdjTiles)
            {
                var tileWithScore = new TileWithScores(new Vector2Int(pat.x, pat.y), startPosition, targetPosition, currentTile);
                if (closedList.Contains(tileWithScore))
                    continue;

                if (!openedList.Contains(tileWithScore))
                {
                    openedList.Add(tileWithScore);
                }
                else
                {
                    //Debug.Log("TileWithScore in Opened List");
                    var twsInOpened = openedList.First(t => t.Equals(tileWithScore));
                    if (tileWithScore.Score < twsInOpened.Score)
                        twsInOpened.ParentTile = currentTile;
                }
            }

        } while (openedList.Count > 0);

        resultDirection = nextPointPosition - startPosition;

        return resultDirection;
    }

    public Vector2Int GetNextPointDirection(int startX, int startY, int targetX, int targetY)
    {
        return GetNextPointDirection(new Vector2Int(startX, startY), new Vector2Int(targetX, targetY));
    }

    public Vector2Int GetNextPointDirection(float startX, float startY, float targetX, float targetY)
    {
        return GetNextPointDirection(new Vector2Int((int)startX, (int)startY), new Vector2Int((int)targetX, (int)targetY));
    }
}

public class TileWithScores
{
    public TileWithScores ParentTile { get; set; }
    public Vector2Int Position { get; set; }
    public int G { get; set; }  // Distance from start (in steps)
    public int H { get; set; }  // Heuristic distance to target ("manhattan distance")
    public int Score => G + H; // Score of G + H

    public TileWithScores(Vector2Int position, Vector2Int startPos, Vector2Int targetPos, TileWithScores parentTile = null)
    {
        Position = position;
        if (parentTile != null)
            ParentTile = parentTile;

        var v_fromStart = startPos - position;
        G = Mathf.Abs(v_fromStart.x) + Mathf.Abs(v_fromStart.y);

        var v_toTarget = targetPos - position;
        H = Mathf.Abs(v_toTarget.x) + Mathf.Abs(v_toTarget.y);
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        TileWithScores other = obj as TileWithScores;
        if (other == null)
            return false;

        return this.Position.x == other.Position.x &&
            this.Position.y == other.Position.y;
    }

    public override int GetHashCode()
    {
        return -425505606 + EqualityComparer<Vector2Int>.Default.GetHashCode(Position);
    }

    public override string ToString()
    {
        return $"[{Position.x},{Position.y}] G:{G} H:{H} Parent:[{ParentTile?.Position.x},{ParentTile?.Position.y}]";
    }
}