﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
    public bool BlockVision;
    public bool BlockMovement;
    public GroundTileType TileType;
}
