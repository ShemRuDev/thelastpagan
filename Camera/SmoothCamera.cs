﻿using UnityEngine;

public class SmoothCamera : MonoBehaviour
{
    public float DampTime = 0.5f;
    public Vector3? Target;

    private Vector3 _velocity = Vector3.zero;

    void Update()
    {
        if (Target != null)
        {
            var camera = GetComponent<Camera>();

            Vector3 point = camera.WorldToViewportPoint(Target.Value);
            Vector3 delta = Target.Value - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref _velocity, DampTime);
        }
    }
}
