﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaling : MonoBehaviour
{
    public float AspectRatio;
    public float Padding;
    public float CameraOffset;

    private BoardManager _board;

    // Start is called before the first frame update
    void Start()
    {
        _board = FindObjectOfType<BoardManager>();
        if (_board != null)
            RepositionCamera(_board.Columns, _board.Rows);
    }

    private void RepositionCamera(int width, int height)
    {
        var tempWidth = width % 2 == 0 ? width / 2 - 0.5f : width / 2;
        var tempHeight = height % 2 == 0 ? height / 2 - 0.5f : height / 2;
        transform.position = new Vector3(tempWidth, tempHeight, CameraOffset);
        //if (_board.Columns >= _board.Rows)
        //    Camera.main.orthographicSize = (_board.Columns / 2 + Padding) / AspectRatio;
        //else
        //    Camera.main.orthographicSize = _board.Rows / 2 + Padding;
    }
}
