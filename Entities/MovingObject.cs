﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
{
    public string Name;

    public static Action<Vector2Int> PlayerMoving;

    public AttackTargetType TargetType = AttackTargetType.Default;

    public float MoveTime = 0.1f;
    public LayerMask BlockingLayer;

    public virtual Camera PlayerCamera { get; }

    private BoxCollider2D _boxCollider;
    private Rigidbody2D _body;
    private float _inverseMoveTime;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        _boxCollider = GetComponent<BoxCollider2D>();
        _body = GetComponent<Rigidbody2D>();
        _inverseMoveTime = 1f / MoveTime;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(_body.position, end, _inverseMoveTime * Time.deltaTime);
            _body.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir, yDir);

        _boxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, BlockingLayer);
        _boxCollider.enabled = true;

        if(hit.transform == null)
        {
            if(PlayerCamera != null)
            {
                PlayerMoving?.Invoke(new Vector2Int((int)end.x, (int)end.y));
                PlayerCamera.GetComponent<SmoothCamera>().Target = new Vector3(end.x, end.y, PlayerCamera.transform.position.z);
            }

            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    // Return true When no Movement Blocks
    protected virtual bool AttemptMove<T>(int xDir, int yDir)
        where T : Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);

        if (hit.transform == null)
            return true;

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
        {
            OnCantMove(hitComponent);
            return false;
        }

        return true;
    }

    protected abstract void OnCantMove<T>(T component)
        where T : Component;
}
