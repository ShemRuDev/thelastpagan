﻿using UnityEngine;

public class Forest : ItemsHolder
{
    public Sprite DamageSprite;
    public int HP = 4;
    public GameObject DropItem;
    
    private SpriteRenderer _spriteRenderer;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void DamageTree(int damage)
    {
        _spriteRenderer.sprite = DamageSprite;
        HP -= damage;
        if (HP <= 0)
        {
            gameObject.SetActive(false);
            HolderDestroyed?.Invoke(new Vector2Int((int)transform.position.x, (int)transform.position.y));
        }
    }

    public override GameObject GetRandomItem()
    {
        if (Random.Range(0, 100) >= 80)
            return DropItem;

        return null;
    }
}
