﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MovingObject
{
    // Main Attributes
    public int DevelopmentPhysical = 50;
    public int DevelopmentIntelligence = 25;
    public int DevelopmentSpirit = 25;

    // Derivative Attrs
    [HideInInspector]
    public PlayerBody Body;
    // -- private
    private int _healthPointsInitial;

    // Shit from RL Tutor
    public int ForestDamage = 1;
    public int PointsForItem = 10;
    public float LevelRestartDelay = 1f;

    private Animator _animator;
    private Camera _playerCamera;
    private InputManager _inputManager;

    private void Awake()
    {
        _playerCamera = Camera.main;    // BAD Practice. (Uses Search by Tag!)

        // ATTENTION!!! ATTRS FORMULAS HERE
        _healthPointsInitial = 20 + DevelopmentPhysical / 2;

        Body = new PlayerBody(_healthPointsInitial);
        PlayerBody.BodyStateChanged += OnPlayerBodyStateChanged;
        OnPlayerBodyStateChanged();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        _animator = GetComponent<Animator>();
        _inputManager = GetComponent<InputManager>();

        // TODO: Don't forget to save Player State Info between Scenes in GameManager
        //_hp = GameManager.Instance.PlayerHP;

        InputManager.Act_PlayerMove += OnPlayerMove;

        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CurrentGameState != GameState.PlayerTurn)
            return;

        //int horizontal = 0;
        //int vertical = 0;

        //horizontal = (int)Input.GetAxisRaw("Horizontal");
        //vertical = (int)Input.GetAxisRaw("Vertical");

        _inputManager.ProcessingInput();
    }

    private void OnPlayerMove(int horizontal, int vertical)
    {
        // NO Diagonal movement
        if (horizontal != 0)
            vertical = 0;

        // Yeah Very Stupid System.
        // That comes from Unity RL Tutorial. Reason behind this code that there was 
        // ONLY 1 type of Blocking tile to Check against! We have much More ---> TODO: REIMPLEMENT Movement System.
        if (AttemptMove<Forest>(horizontal, vertical))
            AttemptMove<Enemy>(horizontal, vertical);
        GameManager.Instance.CurrentGameState = GameState.NPCsTurn;
    }

    private void OnPlayerBodyStateChanged()
    {
        GameManager.Instance.UIManager.UpdateBodyState(Body.InitialBodyState, Body.BodyState);
    }

    public override Camera PlayerCamera => _playerCamera;

    private void OnDisable()
    {
        // Save state here (for example)
        //GameManager.Instance.PlayerHP = _hp;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // here we can restart level (Add IsTrigger and tag "Exit" to all border tiles)
        //if(other.tag == "Exit")
        //{
        //    Invoke("Restart", LevelRestartDelay);
        //    enabled = false;
        //}
        if (other.tag == "Item")
        {
            Debug.Log("Got Woods");
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
    }

    public void LoseHP(AttackTargetType targetType, int loss, string sourceName)
    {
        // HP Loss = 1 for now.

        BodyPart partToDamage = CombatProbabilities.GetRandomBodyPart(targetType);

        Debug.Log($"{sourceName} яростно укусил героя в {partToDamage}");

        Body.DamagePart(partToDamage);

        //_animator.SetTrigger("playerHit"); // TODO: Create Animation for Player Takes damage!
    }

    protected override bool AttemptMove<T>(int xDir, int yDir)
    {
        bool moveResult = base.AttemptMove<T>(xDir, yDir);
        return moveResult;
    }

    protected override void OnCantMove<T>(T component)
    {
        Forest hitForest = component as Forest;

        if (hitForest != null)
        {
            _animator.SetTrigger("playerChop");
            hitForest.DamageTree(ForestDamage);
            return;
        }

        Enemy hitEnemy = component as Enemy;
        if (hitEnemy != null)
        {
            _animator.SetTrigger("playerHit");
            hitEnemy.TakeDamage();
            return;
        }
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
