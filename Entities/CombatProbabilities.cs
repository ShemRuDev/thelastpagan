﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class CombatProbabilities
{
    // For now we are not splitting parts on Source Attack Direction. No Back/Front

    public static readonly Dictionary<BodyPart, int> PlayerBodyPartsHealthDistribution = new Dictionary<BodyPart, int>
    {
        { BodyPart.Head, 7 },
        { BodyPart.Neck, 2 },
        { BodyPart.Chest, 10 },
        { BodyPart.Belly, 8 },
        { BodyPart.Back, 17 },
        { BodyPart.Shoulder, 4 },
        { BodyPart.Forearm, 3 },
        { BodyPart.Wrist, 2 },
        { BodyPart.Hip, 9 },
        { BodyPart.Shin, 7 },
        { BodyPart.Foot, 3 }
    };

    public static readonly Dictionary<BodyPart, int> DefaultPartsProbabilities = new Dictionary<BodyPart, int>
    {
        { BodyPart.Head, 7 },
        { BodyPart.Neck, 2 },
        { BodyPart.Chest, 10 },
        { BodyPart.Belly, 8 },

        { BodyPart.Back, 17 },

        { BodyPart.LeftShoulder, 4 },
        { BodyPart.RightShoulder, 4 },
        { BodyPart.LeftForearm, 3 },
        { BodyPart.RightForearm, 3 },
        { BodyPart.LeftWrist, 2 },
        { BodyPart.RightWrist, 2 },

        { BodyPart.LeftHip, 9 },
        { BodyPart.RightHip, 9 },
        { BodyPart.LeftShin, 7 },
        { BodyPart.RightShin, 7 },
        { BodyPart.LeftFoot, 3 },
        { BodyPart.RightFoot, 3 }
    };

    public static readonly Dictionary<BodyPart, int> AirPartsProbabilities = new Dictionary<BodyPart, int>
    {
        { BodyPart.Head, 15 },
        { BodyPart.Neck, 4 },
        { BodyPart.Chest, 21 },
        { BodyPart.Back, 21 },
        { BodyPart.LeftShoulder, 8 },
        { BodyPart.RightShoulder, 8 },
        { BodyPart.LeftForearm, 7 },
        { BodyPart.RightForearm, 7 },
        { BodyPart.LeftWrist, 4 },
        { BodyPart.RightWrist, 4 }
    };

    public static readonly Dictionary<BodyPart, int> GroundPartsProbabilities = new Dictionary<BodyPart, int>
    {
        { BodyPart.Belly, 8 },
        { BodyPart.Back, 12 },
        { BodyPart.LeftWrist, 2 },
        { BodyPart.RightWrist, 2 },
        { BodyPart.LeftHip, 18 },
        { BodyPart.RightHip, 18 },
        { BodyPart.LeftShin, 14 },
        { BodyPart.RightShin, 14 },
        { BodyPart.LeftFoot, 6 },
        { BodyPart.RightFoot, 6 }
    };

    public static BodyPart GetRandomBodyPart(AttackTargetType targetType)
    {
        var dictionaryToUse = DefaultPartsProbabilities;
        if (targetType == AttackTargetType.Air)
            dictionaryToUse = AirPartsProbabilities;
        else if (targetType == AttackTargetType.Ground)
            dictionaryToUse = GroundPartsProbabilities;

        int rollLimit = dictionaryToUse.Values.Sum();
        int roll = Random.Range(0, rollLimit);

        int accumulatedProb = 0;
        foreach (var bpProb in dictionaryToUse)
        {
            accumulatedProb += bpProb.Value;
            // FOUND
            if (roll < accumulatedProb)
                return bpProb.Key;
        }

        Debug.LogWarning("Probabilities Not Working!");
        return BodyPart.Heart;
    }
}
