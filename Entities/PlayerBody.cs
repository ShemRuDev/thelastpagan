﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerBody
{
    public static Action BodyStateChanged;

    // Part name, Part type, Current State. 0 - Destroyed
    public Dictionary<BodyPart, int> InitialBodyState;
    public Dictionary<BodyPart, int> BodyState;

    public PlayerBody(int hpTotal)
    {
        InitialBodyState = new Dictionary<BodyPart, int>();
        InitialBodyState.Add(BodyPart.Head,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Head] / 100f));
        InitialBodyState.Add(BodyPart.Neck,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Neck] / 100f));
        InitialBodyState.Add(BodyPart.Chest,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Chest] / 100f));
        InitialBodyState.Add(BodyPart.Belly,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Belly] / 100f));
        InitialBodyState.Add(BodyPart.Back,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Back] / 100f));
        // Left hand & leg
        InitialBodyState.Add(BodyPart.LeftShoulder,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Shoulder] / 100f));
        InitialBodyState.Add(BodyPart.LeftForearm,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Forearm] / 100f));
        InitialBodyState.Add(BodyPart.LeftWrist,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Wrist] / 100f));
        InitialBodyState.Add(BodyPart.LeftHip,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Hip] / 100f));
        InitialBodyState.Add(BodyPart.LeftShin,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Shin] / 100f));
        InitialBodyState.Add(BodyPart.LeftFoot,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Foot] / 100f));

        // Right hand & leg
        InitialBodyState.Add(BodyPart.RightShoulder,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Shoulder] / 100f));
        InitialBodyState.Add(BodyPart.RightForearm,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Forearm] / 100f));
        InitialBodyState.Add(BodyPart.RightWrist,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Wrist] / 100f));
        InitialBodyState.Add(BodyPart.RightHip,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Hip] / 100f));
        InitialBodyState.Add(BodyPart.RightShin,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Shin] / 100f));
        InitialBodyState.Add(BodyPart.RightFoot,
            Mathf.RoundToInt(hpTotal * CombatProbabilities.PlayerBodyPartsHealthDistribution[BodyPart.Foot] / 100f));

        // Always one Shot if Heart
        InitialBodyState.Add(BodyPart.Heart, 1);

        BodyState = new Dictionary<BodyPart, int>(InitialBodyState);
    }

    public void DamagePart(BodyPart partKey, int damage = 1)
    {        
        if (!BodyState.ContainsKey(partKey))
            throw new KeyNotFoundException($"Part \"{partKey}\" was not found!");

        BodyState[partKey] -= damage;
        if (BodyState[partKey] < 0)
            BodyState[partKey] = 0;

        GameManager.Instance.UIManager.AddAffect(partKey);

        BodyStateChanged?.Invoke();

        if (BodyState[partKey] == 0)
            GameManager.Instance.GameOver();
    }
}
