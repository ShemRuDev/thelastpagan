﻿using System.Collections;
using System.Collections.Generic;
using Stopwatch = System.Diagnostics.Stopwatch;
using UnityEngine;

public class Enemy : MovingObject
{
    public int DamageToPlayer;
    public bool IsAggressive;
    public int Health;

    //private Animator
    private Transform _target;
    private bool _skipMove; // TODO: Think about different Speed
    private AStarPathfinding _aiPath;
    private FOV _playerFov; // JustThink: Maybe we need VisionDistance instead of using PlayerFOV

    protected override void Start()
    {
        _target = GameManager.Instance.Player.transform;
        _aiPath = GameManager.Instance.BoardScript.Pathfinding;
        _playerFov = GameManager.Instance.FOVPanel.GetComponent<FOV>();

        base.Start();
    }

    public void MoveEnemy()
    {
        Vector2Int nextPointDirection = Vector2Int.zero;

        // If Monster in Player FOV let animation Play
        bool isInPlayerFov = _playerFov.IsInFOV(transform.position);
        if (isInPlayerFov)
            MoveTime = 0.1f;
        else MoveTime = 0;

        // A* pathfinding. Use it only when Enemy IsAggressive && Enemy IN Player FOV
        // else - Stupid random move or no move at all.
        if (IsAggressive && isInPlayerFov)
        {
            //var sw = new Stopwatch();
            //sw.Start();
            nextPointDirection = _aiPath.GetNextPointDirection(transform.position.x, transform.position.y,
                _target.position.x, _target.position.y);
            //sw.Stop();
            //Debug.Log($"Pathfinding Taken: {sw.ElapsedMilliseconds} ms.");
        }
        else
        {
            var stupidRoll = Random.Range(0, 100);
            // So that means that we have 50% prob for NOT Moving at all
            if (stupidRoll > 50)
            {
                // left & right has more prob bcz we have more Columns than Rows (X > Y)
                if (stupidRoll <= 64)
                    nextPointDirection = Vector2Int.left;
                else if (stupidRoll > 64 && stupidRoll <= 79)
                    nextPointDirection = Vector2Int.right;
                else if (stupidRoll > 79 && stupidRoll <= 89)
                    nextPointDirection = Vector2Int.up;
                else nextPointDirection = Vector2Int.down;
            }

            //Debug.Log($"{Name} taken stupid move: {nextPointDirection}");
        }

        AttemptMove<Player>(nextPointDirection.x, nextPointDirection.y);
    }

    protected override bool AttemptMove<T>(int xDir, int yDir)
    {
        //if (_skipMove)
        //{
        //    _skipMove = false;
        //    return;
        //}

        return base.AttemptMove<T>(xDir, yDir);

        //_skipMove = true;
    }

    protected override void OnCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;

        hitPlayer.LoseHP(TargetType, DamageToPlayer, Name);
    }

    public void TakeDamage()
    {
        Health--;
        IsAggressive = true;

        if (Health <= 0)
        {
            gameObject.SetActive(false);
            GameManager.Instance.RemoveEnemy(Name);
            Debug.Log($"{Name} killed");

            Destroy(gameObject);
        }
    }
}
