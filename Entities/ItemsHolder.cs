﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ItemsHolder : MonoBehaviour
{
    public static Action<Vector2Int> HolderDestroyed;
    public abstract GameObject GetRandomItem();
}
