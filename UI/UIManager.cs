﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject BodyState;
    public GameObject AffectTextPrefab;

    private bool _panelBodyState_active = false;

    // Use this for initialization
    void Start()
    {
        BodyState.SetActive(_panelBodyState_active);
    }

    public void ToggleBodyState()
    {
        _panelBodyState_active = !_panelBodyState_active;
        BodyState.SetActive(_panelBodyState_active);
    }

    public void UpdateBodyState(Dictionary<BodyPart, int> initialState, Dictionary<BodyPart, int> currentState)
    {
        BodyState.transform.Find("panelBody/head/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.Head]}/{initialState[BodyPart.Head]}";
        BodyState.transform.Find("panelBody/neck/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.Neck]}/{initialState[BodyPart.Neck]}";
        BodyState.transform.Find("panelBody/left_arm/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.LeftShoulder] + currentState[BodyPart.LeftForearm]}/{initialState[BodyPart.LeftShoulder] + initialState[BodyPart.LeftForearm]}";
        BodyState.transform.Find("panelBody/right_arm/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.RightShoulder] + currentState[BodyPart.RightForearm]}/{initialState[BodyPart.RightShoulder] + initialState[BodyPart.RightForearm]}";
        BodyState.transform.Find("panelBody/left_hand/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.LeftWrist]}/{initialState[BodyPart.LeftWrist]}";
        BodyState.transform.Find("panelBody/right_hand/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.RightWrist]}/{initialState[BodyPart.RightWrist]}";
        BodyState.transform.Find("panelBody/chest/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.Chest]}/{initialState[BodyPart.Chest]}";
        BodyState.transform.Find("panelBody/belly/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.Belly]}/{initialState[BodyPart.Belly]}";
        BodyState.transform.Find("panelBody/left_leg/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.LeftHip] + currentState[BodyPart.LeftShin]}/{initialState[BodyPart.LeftHip] + initialState[BodyPart.LeftShin]}";
        BodyState.transform.Find("panelBody/right_leg/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.RightHip] + currentState[BodyPart.RightShin]}/{initialState[BodyPart.RightHip] + initialState[BodyPart.RightShin]}";
        BodyState.transform.Find("panelBody/left_foot/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.LeftFoot]}/{initialState[BodyPart.LeftFoot]}";
        BodyState.transform.Find("panelBody/right_foot/state").GetComponent<TextMeshProUGUI>().text =
            $"{currentState[BodyPart.RightFoot]}/{initialState[BodyPart.RightFoot]}";
    }

    public void AddAffect(BodyPart bodyPart)
    {
        string parentPanelPath = string.Empty;
        string additionalInfo = string.Empty;

        switch (bodyPart)
        {
            case BodyPart.Head:
                parentPanelPath = "panelDetails/headDetails";
                break;
            case BodyPart.Neck:
                parentPanelPath = "panelDetails/neckDetails";
                break;
            case BodyPart.Chest:
            case BodyPart.Heart:
                parentPanelPath = "panelDetails/chestDetails";
                if (bodyPart == BodyPart.Heart)
                    additionalInfo = "прямо в сердце";
                break;
            case BodyPart.Belly:
                parentPanelPath = "panelDetails/bellyDetails";
                break;
            case BodyPart.Back:
                parentPanelPath = "panelDetails/backDetails";
                break;
            case BodyPart.Shoulder:
            case BodyPart.LeftShoulder:
            case BodyPart.RightShoulder:
            case BodyPart.Forearm:
            case BodyPart.LeftForearm:
            case BodyPart.RightForearm:
            case BodyPart.Wrist:
            case BodyPart.LeftWrist:
            case BodyPart.RightWrist:
                parentPanelPath = "panelDetails/armsDetails";
                if (bodyPart == BodyPart.LeftShoulder)
                    additionalInfo = "левого плеча";
                else if (bodyPart == BodyPart.LeftForearm)
                    additionalInfo = "левого предплечья";
                else if (bodyPart == BodyPart.LeftWrist)
                    additionalInfo = "левой кисти";
                else if (bodyPart == BodyPart.RightShoulder)
                    additionalInfo = "правого плеча";
                else if(bodyPart == BodyPart.RightForearm)
                    additionalInfo = "правого предплечья";
                else if (bodyPart == BodyPart.RightWrist)
                    additionalInfo = "правой кисти";
                break;
            case BodyPart.Hip:
            case BodyPart.LeftHip:
            case BodyPart.RightHip:
            case BodyPart.Shin:
            case BodyPart.LeftShin:
            case BodyPart.RightShin:
            case BodyPart.Foot:
            case BodyPart.LeftFoot:
            case BodyPart.RightFoot:
                parentPanelPath = "panelDetails/legsDetails";
                if (bodyPart == BodyPart.LeftHip)
                    additionalInfo = "левого бедра";
                else if(bodyPart == BodyPart.LeftShin)
                    additionalInfo = "левой голени";
                else if (bodyPart == BodyPart.LeftFoot)
                    additionalInfo = "левой ступни";
                else if (bodyPart == BodyPart.RightHip)
                    additionalInfo = "правого бедра";
                else if(bodyPart == BodyPart.RightShin)
                    additionalInfo = "правой голени";
                else if (bodyPart == BodyPart.RightFoot)
                    additionalInfo = "правой ступни";
                break;
            default:
                break;
        }

        var parent = BodyState.transform.Find(parentPanelPath);
        var affect = Instantiate(AffectTextPrefab, parent);

        if (!string.IsNullOrEmpty(additionalInfo))
            affect.GetComponent<TextMeshProUGUI>().text += " " + additionalInfo;
    }
}
