﻿using UnityEngine;
using System.Collections;
using System;

public class InputManager : MonoBehaviour
{
    public static Action<int, int> Act_PlayerMove;

    public void ProcessingInput()
    {
        int horizontalMove = (int)Input.GetAxisRaw("Horizontal");
        int verticalMove = (int)Input.GetAxisRaw("Vertical");
        if (horizontalMove != 0 || verticalMove != 0)
            Act_PlayerMove?.Invoke(horizontalMove, verticalMove);

        if (Input.GetKeyDown(KeyCode.B))
            GameManager.Instance.UIManager.ToggleBodyState();
    }
}
