﻿using UnityEngine;
using System.Collections;

public enum BodyPart
{
    Head = 0,
    Neck,
    Chest,
    Heart,
    Belly,
    Back,
    // Hand parts
    Shoulder,
    LeftShoulder,
    RightShoulder,
    Forearm,
    LeftForearm,
    RightForearm,
    Wrist,
    LeftWrist,
    RightWrist,

    // Leg parts
    Hip,
    LeftHip,
    RightHip,
    Shin,
    LeftShin,
    RightShin,
    Foot,
    LeftFoot,
    RightFoot
}
