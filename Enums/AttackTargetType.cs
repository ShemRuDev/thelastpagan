﻿using UnityEngine;
using System.Collections;

public enum AttackTargetType
{
    /// <summary>
    ///     This one is a middle. Like Human. Can target all body parts.
    /// </summary>
    Default,
    /// <summary>
    ///     Air units. Target mainly Upper body parts.
    /// </summary>
    Air,
    /// <summary>
    ///     Ground units. Target mainly Bottom body parts.
    /// </summary>
    Ground
}
