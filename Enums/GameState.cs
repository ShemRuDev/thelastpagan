﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    PlayerTurn = 0,
    NPCsTurn = 10
}
