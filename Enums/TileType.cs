﻿public enum GroundTileType
{
    GroundEmpty = 0,
    GroundDirtS,
    GroundDirtM,
    GroundDirtL,
    GroundDirtXL,
    GroundGrassS,
    GroundGrassAndFlowers,
    GroundGrassM,
    ForestOak,
    ForestFir,
    WaterFull,
    RiverStart,
    River,

    Unknown = 1000
}
